import operator

import numpy as np
import pandas as pd
from scipy.stats import norm

from classifier_base import ClassifierBase


class NBayes(ClassifierBase):
    def __init__(self):
        self._means = {}
        self._sigmas = {}

        self.y_probabilities = None
        self._features_len = None

    def predict(self, x):
        if type(x) == pd.DataFrame:
            x = x.as_matrix()

        return [self._predict_result(x_) for x_ in x]

    def fit(self, X, y):
        super().fit(X, y)

        self._features_len = X.shape[1]

        self.y_probabilities = self._get_class_probabilities()
        self._set_means()
        self._set_sigmas()

    def _predict_result(self, x):
        posteriors = {}

        for c_label in self.classes_:
            prob = np.prod([self._get_likelihood_of_value_given_class(x_i, feature_index=i, y_label=c_label)
                            for i, x_i in enumerate(x)])

            posteriors[c_label] = self.y_probabilities[c_label] * prob

        return max(posteriors.items(), key=operator.itemgetter(1))[0]

    def _get_class_probabilities(self):
        return {c_name: np.sum(self.y_ == c_name) / len(self.y_) for c_name in self.classes_}

    def _get_likelihood_of_value_given_class(self, x, feature_index, y_label):
        return norm.pdf(x, loc=self._means[y_label][feature_index], scale=self._sigmas[y_label][feature_index])

    def _set_sigmas(self):
        for class_name in self.classes_:
            self._sigmas[class_name] = [np.std(self.X_[self.y_ == class_name][:, feature], ddof=0)
                                        for feature in range(self._features_len)]

    def _set_means(self):
        for class_name in self.classes_:
            self._means[class_name] = [np.mean(self.X_[self.y_ == class_name][:, feature])
                                       for feature in range(self._features_len)]

