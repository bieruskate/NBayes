from sklearn import datasets
from sklearn.model_selection import cross_val_score
import numpy as np

from nbayes import NBayes

if __name__ == '__main__':
    wine = datasets.load_wine()
    X = wine.data
    y = wine.target

    bayes = NBayes()
    bayes.fit(X, y)

    cv = cross_val_score(bayes, X, y, cv=10, scoring='f1_micro')

    print(f'Overall F1 score: {np.mean(cv)}')
